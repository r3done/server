//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

const mongoose  = require("mongoose");
// Require the dev-dependencies
const chai      = require('chai');
const chaiHttp  = require('chai-http');
const should    = chai.should();
// Require local resources
const Videos    = require('../models/videos');
const Users     = require('../models/user');
const server    = require('../app');
const auth      = require('./auth');

// Setup
chai.use(chaiHttp);

//Our parent block
describe('Videos', () => {

  let userToken;

  before((done) => { //Before each test we empty the database

    Promise.all([
      Videos.removeAsync({}),
      Users.removeAsync({}),
      auth.registerAndLogin({
        userName: 'danieltmbr',
        firstName: 'Daniel',
        lastName: 'Tombor',
        password: 'Asdf123.',
        role: 'user'
      })
    ])
    .spread(function(video, user, token) {
      if (video == null || user == null || token == null) {
        throw new Error("Couldn't perform action");
      } else {
        userToken = token;
      }
      done();
    })
    .catch(error => {
      throw new Error("Couldn't perform action");
      done();
    });


    // Users.remove({}), (err) => { console.log("Done removing users"); });
    // Videos.remove({}), (err) => { console.log("Done removing videos"); });
    // auth.registerAndLogin({
    //   userName: 'danieltmbr',
    //   firstName: 'Daniel',
    //   lastName: 'Tombor',
    //   password: 'Asdf123.',
    //   role: 'user',
    //   callback: (error, token) => {
    //     if (error) done();
    //     userToken = token;
    //   }
    // });
  });


  /*
   * Test the /GET route
   */
  describe('/GET videos', () => {

      it('it should GET all the videos', (done) => {
        chai.request(server)
            .get('/videos')
            .end((err, res) => {
                res.should.have.status(200);
                res.body.should.be.a('array');
                res.body.length.should.be.eql(0);
              done();
            });
      });
  });

  describe('/POST video', () => {

      it('it should not POST a video without youtubeVideo, music, choreographerId and imageURL fields, since they\'re required', (done) => {

        let video = {
            video: {url:"https://www.youtube.com/watch?v=B8lb9MwBzl8", id:"B8lb9MwBzl8"},
            image: "https://68.media.tumblr.com/29969083a14a2f580f2197f8d9d28b3d/tumblr_nzuk8379TC1qa0rryo1_1280.jpg",
            description: "R3D ONE Advanced Class Footage\nAssisted by Efri Demissie\nCamera: Agota Pocsai\nEdited: Duc Anh Tran",
            music: { title: "HARAMBE", artist: "Dumbfoundead"}
        }

        chai.request(server)
            .post('/videos')
            .send(video)
            .end((err, res) => {
                res.should.have.status(500);
                res.body.should.be.a('object');
                res.body.should.have.property('message').eql("Video validation failed");
              done();
            });
      });
  });

});
