const passport = require('passport');
const mongoose = require('mongoose');
const Users  = require('../models/user');
const Verify = require('../routes/verify');
const config = require('../config/dev');
const Promise  = require('bluebird');

module.exports.registerAndLogin = function({userName, firstName, lastName, password, role}) {

  return new Promise(function(resolve, reject) {

    Users.register(
      new Users({
        username: userName,
        name: {
          firstName: firstName,
          lastName: lastName,
        },
        role: role
      }),
      password,
      function(error, user) {
        if (error) { reject(error); }
        req.logIn(user, function(error) {
          if (error) { reject(error); }
          else { resolve(Verify.getToken(user)) }
        });
    });

  });

};
