const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// Import Action Schemas
const Actions = require('../models/actions');
const ActionSchema = mongoose.model('VideoAction').schema;
const ObjectId = Schema.Types.ObjectId;

// Will add the Currency type to the Mongoose Schema types
require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;

const scheduleTimeSchema = new Schema({
  hour: {
    type: Number,
    required: true
  },
  minute: Number
})

// Schedule Schema
const scheduleSchema = new Schema({
  day: {
    type: String,
    required: true,
    enum: ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']
  },
  startTime: {
    type: scheduleTimeSchema,
    required: true
  },
  endTime: {
    type: scheduleTimeSchema,
    required: true
  }
})

// Course Schema
const courseSchema = new Schema({
  title: {
    type: String,
    required: true
  },
  image: {
    type: String,
  },
  choreographer: {
    type: ObjectId,
    ref: 'User'
  },
  location: {
    type: ObjectId,
    ref: 'Location'
  },
  schedules: [scheduleSchema],
  price: {
    type: Currency,
    required: true
  },
  level: {
    type: String,
    required: true,
    enum: ['Beginner', 'Intermediate', 'Upperintermediate', 'Advanced']
  },
  description: {
    type: String
  },
  videos: [{type: Schema.Types.ObjectId, ref: 'Video'}],
  members: [ActionSchema],
  order: {
    type: Number,
    required: true
  }
}, {
    timestamps: true
})

const Course = mongoose.model('Course', courseSchema);

// make this available to our Node application
module.exports = Course;
