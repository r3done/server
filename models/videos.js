const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// Import Action Schemas
const Actions = require('../models/actions');
const ActionSchema = mongoose.model('VideoAction').schema;
const ObjectId = Schema.Types.ObjectId;

// YoutubeVideo schema
const youtubeSchema = new Schema({
	url: {
		type: String
	},
  youtubeId: {
    type: String
  },
	image: {
		type: String
	},
	duration: {
		type: Number
	},
	viewsCount: {
		type: Number,
	}
}, {
	timestamps: true
});

// Music schema
const musicSchema = new Schema({
	artist: {
		type: String
	},
  title: {
    type: String
  }
});

// Video schema
const videoSchema = new Schema({
	video: {
		type: youtubeSchema,
		required: true
	},
  music: {
    type: musicSchema,
    required: true
  },
  choreographer: {
    type: ObjectId,
    ref: 'User',
    required: true
  },
	image: {
		type: String,
		required: true
	},
  releaseDate: {
    type: Date,
    default: Date.now
  },
  description: {
    type: String
  },
  assistants: [{type: ObjectId, ref: 'User' }],
  dancers: [{type: ObjectId, ref: 'User' }],
  likes: [ActionSchema]
}, {
	timestamps: true
});

videoSchema.index(
	{ description: 'text' }
);

var YoutubeVideo = mongoose.model('YoutubeVideo', youtubeSchema);
var Videos = mongoose.model('Video', videoSchema);

// make this available to our Node application
module.exports = Videos;
