const mongoose = require('mongoose');
const Schema = mongoose.Schema;
// Import Action Schemas
const Actions = require('../models/actions');
const ActionSchema = mongoose.model('EventAction').schema;
const ObjectId = Schema.Types.ObjectId;

// Will add the Currency type to the Mongoose Schema types
require('mongoose-currency').loadType(mongoose);
const Currency = mongoose.Types.Currency;

// Event Schema
const eventSchema = new Schema({
  title: {
		type: String,
    required: true
	},
	image: {
		type: String,
    required: true
	},
  type: {
    type: String,
    enum: ['workshop', 'show', 'camp', 'other'],
    required: true
  },
  startDate: {
    type: Date,
    required: true
  },
  endDate: {
    type: Date
  },
  choreographers: [{type: ObjectId,ref: 'User'}],
  location: {
    type: ObjectId,
    ref: 'Location'
  },
  price: {
    type: Currency,
    required: true
  },
  description: {
    type: String
  },
  videos: [{type: ObjectId, ref: 'Video'}],
  interests: [ActionSchema]
}, {
    timestamps: true
})

const Event = mongoose.model('Event', eventSchema);

// make this available to our Node application
module.exports = Event;
