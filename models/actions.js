const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ObjectId = Schema.Types.ObjectId;

// User's video actions schema
const VideoActionSchema = new Schema({
	action: {
		type: String,
		required: true,
		enum: ['like', 'save']
	},
  video: {
    type: ObjectId,
    ref: 'Video',
		required: true
  },
	user: {
    type: ObjectId,
    ref: 'User',
		required: true
  }
}, {
	timestamps: true
});
VideoActionSchema.index({ video: 1, user: 1 });

// User's event actions schema
const EventActionSchema = new Schema({
	action: {
		type: String,
		required: true,
		enum: ['interest']
	},
  event: {
    type: ObjectId,
    ref: 'Event',
		required: true
  },
	user: {
    type: ObjectId,
    ref: 'User',
		required: true
  }
}, {
	timestamps: true
});
EventActionSchema.index({ event: 1, user: 1 });

// User's course actions schema
const CourseActionSchema = new Schema({
	action: {
		type: String,
		required: true,
		enum: ['join']
	},
  course: {
    type: ObjectId,
    ref: 'Course',
		required: true
  },
	user: {
    type: ObjectId,
    ref: 'User',
		required: true
  }
}, {
	timestamps: true
});
CourseActionSchema.index({ course: 1, user: 1 });

// make this available to our Node application
module.exports = {
	VideoAction: mongoose.model('VideoAction', VideoActionSchema),
	EventAction: mongoose.model('EventAction', EventActionSchema),
	CourseAction: mongoose.model('CourseAction', CourseActionSchema)
};
