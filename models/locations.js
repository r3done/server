const mongoose = require('mongoose');
const Schema = mongoose.Schema;

// Address Schema
const addressSchema = new Schema({
	country: {
		type: String
	},
	city: {
		type: String
	},
  zip: {
    type: String
  },
  coordinates: {
    longitude: Number,
    latitude: Number
  },
	address: {
		type: String
	}
})

// create schema
const locationSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	address: addressSchema,
	image: {
		type: String
	}
}, {
	timestamps: true
});

const Location = mongoose.model('Location', locationSchema);

// make this available to our Node application
module.exports = Location;
