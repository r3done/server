const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const passportLocalMongoose = require('passport-local-mongoose');
const ObjectId = Schema.Types.ObjectId;

const teacherSchema = new Schema({
	order: {
		type: Number,
		required: true
	},
	user: {
		type: ObjectId,
		required: true,
		ref: 'userSchema'
	}
});

const userSchema = new Schema({
	username: {
      type: String,
			unique : true,
			required : true,
			dropDups: true
  },
	password: String,
	OauthId: String,
  OauthToken: String,
  name: {
			firstName: String,
			lastName: String
  },
	email: String,
	image: String,
  role: {
      type: String,
			enum: ['user', 'member', 'teacher', 'admin'],
			required: true,
			default: 'user'
  },
	birthday: Date,
	phone: String,
	videos: {
		choreographer: [{
			type: ObjectId,
	    ref: 'Video'
		}],
		assistant: [{
			type: ObjectId,
	    ref: 'Video'
		}],
		dancer: [{
			type: ObjectId,
	    ref: 'Video'
		}],
		saved: [{
			type: ObjectId,
	    ref: 'UserVideoAction'
		}]
	}
});

userSchema.plugin(passportLocalMongoose);

const User = mongoose.model('User', userSchema);
// make this available to our Node application
module.exports = User;
