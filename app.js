const express       = require('express');
const path          = require('path');
const favicon       = require('serve-favicon');
const logger        = require('morgan');
const cookieParser  = require('cookie-parser');
const bodyParser    = require('body-parser');
const mongoose      = require('mongoose');
const blueBird      = require('bluebird');
const passport      = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const config        = require('config'); //we load the db location from the JSON files

//var atuhenticate = require('./authenticate');

//db options
let options = {
  server: { socketOptions: { keepAlive: 1, connectTimeoutMS: 30000 } },
  replset: { socketOptions: { keepAlive: 1, connectTimeoutMS : 30000 } }
};

// Setup Database
mongoose.Promise = blueBird;
blueBird.promisifyAll(mongoose);
mongoose.connect(config.mongoUrl, options);

let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function() {
    // we're connected!
    console.log("Connected correctly to server");
});

// Import Routers
const routes              = require('./routes/index');
const usersRouter         = require('./routes/users');
const videoRouter         = require('./routes/videos');
const eventRouter         = require('./routes/events');
const courseRouter        = require('./routes/courses');
const choreographerRouter = require('./routes/choreographers');
const profileRouter       = require('./routes/profiles');

// Create app
let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views')); // szeretem az asszonyomat. uramat és parancsolómat.
app.set('view engine', 'jade');

// Setup logging -> don't show the log when it is test
if(config.util.getEnv('NODE_ENV') !== 'test') {
    //use morgan to log at command line
    app.use(logger('combined')); //'combined' outputs the Apache style LOGs
}

// uncomment after placing your favicon in /public
// app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());

// Passport config
const User = require('./models/user');
app.use(passport.initialize());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// Public diretory
app.use(express.static(path.join(__dirname, 'public')));

// Setup routers
app.use('/', routes);
app.use('/users', usersRouter);
app.use('/videos', videoRouter);
app.use('/events', eventRouter);
app.use('/courses', courseRouter);
app.use('/choreographers', choreographerRouter);

// Catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.json({
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({
        message: err.message,
        error: {}
    });
});

// Export application
module.exports = app;
