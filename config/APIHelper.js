
const APIHelper = {

  // Constants
  success: {success: "Success", status : 200},

  // Population
  populate: {
    user: 'id name username image',
    location: 'id name address image',
    video: 'id music image'
  },

  // Errors
  errors: {
    action: {
      notFound: new ReferenceError('No action found'),
      unsupported: new Error('Unsupported action'),
      perform: new Error("Couldn't perform action")
    }
  },

  // Methods
  page: function(date) {
    let created = Date()
    if (date) { created = date }
    return { createdAt: { $lt: created } }
  },

  inverseAction: function(action) {
    switch (action) {
      case 'like': return 'unlike';
      case 'unlike': return 'like';
      case 'save': return 'unsave';
      case 'unsave': return 'save';
      case 'follow': return 'unfollow';
      case 'unfollow': return 'follow';
      case 'join': return 'leave';
      case 'leave': return 'join';
      case 'interest': return 'uninterest';
      case 'uninterest': return 'interest';
    }
  },

  wrapList: function(count, list) {
    let c = count != null ? count : 0;
    let l = list != null ? list : [];
    return {'count': c, 'list': l }
  },

  arrayIsEmpty: function(list) {
    return list != null ? !list.length : true
  }
};

module.exports = APIHelper;
