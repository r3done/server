const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const mongoose = require('mongoose');

const Users  = require('../models/user');
const Verify = require('./verify');
const config = require('../config/dev');

let router = express.Router();
router.use(bodyParser.json());

/* GET users listing. */
router.route('/')
.get(
//	Verify.verifyOrdinaryUser,
//	Verify.verifyAdmin,
  function(req, res, next) {

  	Users.find({}, function (err, users) {
  		if (err) return next(err);
  		res.json(users);
  	});
});

router.route('/register')
.post(function(req, res) {

	Users.register(
    new Users({
  		username: req.body.username,
  		name: {
  			firstName: req.body.name.firstName,
  			lastName: req.body.name.lastName,
  		},
  		role: 'user'
  	}),
    req.body.password,
    function(error, user) {
      if (error) return res.status(500).json({err: error});

      req.logIn(user, function(err) {
    		if (err) {
          return res.status(201).json({status: 'Registration successful, please log in!'});
    			// return res.status(500).json({err: 'Could not log in user' });
    		}
    		res.status(200).json({
    			status: 'Registration successful!',
    			success: true,
    			token: Verify.getToken(user)
    		});
    	});
  });
});

router.post('/login', function(req, res, next) {

	passport.authenticate('local', function(err, user, info) {

		if (err) {
			return next(err);
		}
		if (!user) {
			return res.status(401).json({
				err: info
			});
		}
		req.logIn(user, function(err) {
			if (err) {
				return res.status(500).json({
					err: 'Could not log in user'
				});
			}
			res.status(200).json({
				status: 'Login successful!',
				success: true,
				token: Verify.getToken(user)
			});
		});

	})(req, res, next);
});

router.get('/logout', function(req, res) {
	req.logout();

	res.status(200).json({
		status: 'Bye!'
	});
});

// router.get('/facebook', passport.authenticate('facebook'),
//   function(req, res){});
//
// router.get('/facebook/callback', function(req,res,next){
//   passport.authenticate('facebook', function(err, user, info) {
//     if (err) {
//       return next(err);
//     }
//     if (!user) {
//       return res.status(401).json({
//         err: info
//       });
//     }
//     req.logIn(user, function(err) {
//       if (err) {
//         return res.status(500).json({
//           err: 'Could not log in user'
//         });
//       }
//               var token = Verify.getToken(user);
//               res.status(200).json({
//         status: 'Login successful!',
//         success: true,
//         token: token
//       });
//     });
//   })(req,res,next);
// });

module.exports = router;
