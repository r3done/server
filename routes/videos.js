const express     = require('express');
const bodyParser  = require('body-parser');
const mongoose    = require('mongoose');
const Promise     = require('bluebird');

const config      = require('../config/dev');
const Videos      = require('../models/videos');
const Verify      = require('./verify');
const Users       = require('../models/user');
const Actions     = require('../models/actions');
const VideoAction = Actions.VideoAction;
const APIHelper   = require('../config/APIHelper');

// Init Videos Router
let router = express.Router();
router.use(bodyParser.json());

// Setup REST API call handlers for '/videos'
router.route('/')

  // List
  .get(
    Verify.attachUserId,
    function(req, res, next) {

    let query = APIHelper.page(req.query.lastItemDate)
    if (req.params.choreographerIds) {
      query.choreographer = { $in: req.params.choreographerIds }
    }

    Videos.find(query)
      .populate({
        path: 'choreographer',
        select: APIHelper.populate.user
      })
      .sort({ createdAt: 'descending' })
      .limit(config.pageCount)
      .execAsync()
      .then(videos => {
        res.json(videosHelper.map.videoList(videos));
      })
      .catch(error => { return next(error) });
  })

  // Create
  .post(function(req, res, next) {
    Videos.createAsync(req.body)
    .then(video => { res.json(video) })
    .catch(error => { return next(error) });
  })

  // Delete all
  .delete(function(req, res, next) {
    Videos.removeAsync({})
    .then(resp => { res.json(resp) })
    .catch(error => { return next(error) });
  });

router.route('/search')
  // Search for videos by query
  .get(function(req, res, next) {

    let query = APIHelper.page(req.query.lastItemDate)
    if (req.query.q) {
      query.$text = { $search : req.query.q }
      query.score = { $meta: 'textScore' }
    }

    Videos.find(query)
      .sort({ score : { $meta : 'textScore' } })
      .limit(config.pageCount)
      .execAsync()
      .then(videos => { res.json(videosHelper.map.videoList(videos)); })
      .catch(error => { return next(error) });
  });

// TODO: Remove - It's just for testing !!!! REMOVE
router.route('/actions')

    .get(function(req, res, next) {
      VideoAction.find()
      .then(actions => { res.json(actions) })
      .catch(error => { return next(error) });
    });

router.route('/actions/remove')

      .delete(function(req, res, next) {
        VideoAction.remove()
        .then(resp => { res.json(resp) })
        .catch(error => { return next(error) });
  });

// Setup REST API call handlers for '/videos/:id' - Single object by id
router.route('/:id')

  // Get video
  .get(function(req, res, next) {

    let assistantsCount = 0;
    let dancersCount = 0;
    let likesCount = 0;

    Videos.findById(req.params.id)
    .populate('choreographer')
    .execAsync()
    .then(video => {

      assistantsCount = video.assistants.length;
      dancersCount = video.dancers.length;
      likesCount = video.likes.length;

      return video
          .populate({
            path: 'assistants',
            select: APIHelper.populate.user,
            options: { limit: config.pageCount }
          })
          .populate({
            path: 'dancers',
            select: APIHelper.populate.user,
            options: { limit: config.pageCount }
          })
          .execPopulate();
          // .populate({
          //   likes: 'likes',
          //   select: 'user',
          //   match: { user: userId }
          //
          // })
    })
    .then(video => {
      video.assistantsCount = video.assistants.length;
      video.dancersCount = video.dancers.length;
      video.likesCount = video.likes.length;
      res.json(videosHelper.map.video(video));
    })
    .catch(error => { return next(error) });
  })

  // Update video
  .put(function(req, res, next) {
    Videos.findByIdAndUpdate(
      req.params.id,
      { $set: req.body },
      { new: true}
    )
    .then(video => { res.json(video) })
    .catch(error => { return next(error) });
  })

  // Delete video
  .delete(function(req, res, next) {
    Videos.findByIdAndRemove(req.params.id)
    .then(resp => { res.json(resp) })
    .catch(error => { return next(error) });
  });

router.route('/:id/action')
  .post(
    Verify.verifyOrdinaryUser,
    function(req, res, next) {

      switch (req.body.action) {
        case 'like':
        case 'save':
          videosHelper.insertAction(
            req.body.action, // Action
            req.params.id, // Video id
            req.decoded._doc._id // User id
          )
          .then(object => { res.json(APIHelper.success); })
          .catch(error => { return next(error); });
          break;
        case 'unlike':
        case 'unsave':
          videosHelper.removeAction(
            req.body.action, // Action
            req.params.id, // Video id
            req.decoded._doc._id // User id
          )
          .then(object => { res.json(APIHelper.success); })
          // If Reference Error: There was no action to undo -> So item is unliked ;)
          // TODO: Megfelelő státuszú response
          .catch(ReferenceError, function(e) { res.json(APIHelper.success); })
          .catch(error => { return next(error); });
          break;
        default:
          var err = new Error('Unsupported action');
          err.status = 500;
          return next(err);
      }
      return;
  });

router.route('/:id/likes')
  .get(
    Verify.verifyOrdinaryUser,
    function(req, res, next) {
    var likesCount = 0
    Videos.findByIdAsync(req.params.id)
      .then(video => {

        likesCount = video.likes.length;
        return video
        .populate({
          path: 'likes',
          select: 'user',
          match: { action: 'like' },
          options: { limit: config.pageCount }
        })
        .populate({
          path: 'likes.user',
          select: APIHelper.populate.user
        });
      })
      .then(video => { res.json(APIHelper.wrapList(likesCount, video.likes)); })
      .catch(error => { return next(error) });
  });

module.exports = router;

// For PUT methods use Object.assign
// e.g.:
/*
 * PUT /book/:id to updatea a book given its id
 */
// function updateBook(req, res) {
//     Book.findById({_id: req.params.id}, (err, book) => {
//         if(err) res.send(err);
//         Object.assign(book, req.body).save((err, book) => {
//             if(err) res.send(err);
//             res.json({ message: 'Book updated!', book });
//         });
//     });
// }

// Private Helper Methods
const videosHelper = {

  // DB Methods
  insertAction: function(userAction, videoId, userId) {

    return VideoAction.findOneAsync({
      user: userId,
      video: videoId,
      action: userAction
    })
    .then(action => {
      // If action has been inserted already -> Return success
      if (action != null) { return Promise.resolve(); }
      // Else insert a new action
      return Promise.all([
        Videos.findByIdAsync(videoId),
        Users.findByIdAsync(userId),
        VideoAction.createAsync({
          video: videoId,
          user: userId,
          action: userAction
        })
      ])
      .spread(function(video, user, action) {
        if (video == null || user == null || action == null) {
          // TODO: Rollback
          throw new Error("Couldn't perform action");
        }
        switch (userAction) {
          case 'like':
            video.likes.push(action);
            return video.saveAsync();
          case 'save':
            user.videos.saved.push(action);
            return user.saveAsync();
          default: return new Promise.resolve();
        }
      });
    });
  },

  removeAction: function(userAction, videoId, userId) {

    return Promise.all([
      Videos.findByIdAsync(videoId),
      Users.findByIdAsync(userId),
      VideoAction.findOneAndRemoveAsync({
        user: userId,
        video: videoId,
        action: APIHelper.inverseAction(userAction)
      })
    ])
    .spread(function(video, user, action) {
      if (action == null) { throw new ReferenceError('No action found'); }
      if (video == null || user == null) { throw new Error("Couldn't perform action"); }

      switch (userAction) {
        case 'unlike':
          video.likes.pull(action._id);
          return video.saveAsync();
        case 'unsave':
          user.videos.saved.pull(action._id);
          return user.saveAsync();
        default: return new Promise.resolve();
      }
    });
  },

  map: {

    videoList: function(videos) {
      return videos.map( function(model) {
        let video = model.toObject();
        delete video['assistants'];
        delete video['dancers'];
        delete video['description'];
        delete video['__v'];
        return video
      });
    },

    video: function(videoModel) {
      let video = videoModel.toObject();

      video.assistants = APIHelper.wrapList(videoModel.assistantsCount, videoModel.assistants);
      video.dancers = APIHelper.wrapList(videoModel.dancersCount, videoModel.dancers);
      video.likes = APIHelper.wrapList(videoModel.likesCount, videoModel.likes);

      video.isLiked = !APIHelper.arrayIsEmpty(videoModel.userLiked);
      video.isSaved = !APIHelper.arrayIsEmpty(videoModel.userSaved);

      delete video['__v'];
      return video
    }
  }

};
