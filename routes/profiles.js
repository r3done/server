const express = require('express');
const bodyParser = require('body-parser');
const passport = require('passport');
const mongoose = require('mongoose');

const config = require('../config/dev');
const Users  = require('../models/user');

const router = express.Router();
router.use(bodyParser.json());

/* GET users listing. */
router.route('/')

  .get(
    // Verify.verifyOrdinaryUser,
    // Verify.verifyAdmin,)
    function(req, res, next) {

      Users.find({}, function(err, users) {
        console.log(users)
        if (err) return next(err);
        res.json(users);
      });
    });

// Setup REST API call handlers for '/profiles/:id' - Single object by id
router.route('/:id')

  // Get event
  .get(function(req, res, next) {
    Users.findById(req.params.id)
      .populate({
        path: 'videos.choreographer',
        select: 'id music.artist music.title image',
        options: {
          limit: 5
        }
      })
      .populate({
        path: 'videos.assistant',
        select: 'id music.artist music.title image',
        options: {
          limit: 5
        }
      })
      .populate({
        path: 'videos.dancer',
        select: 'id music.artist music.title image',
        options: {
          limit: 5
        }
      })
      .populate({
        path: 'videos.saved',
        select: 'id music.artist music.title image',
        options: {
          limit: 5
        }
      })
      // TODO: Number of interests
      .exec(function(err, event) {
        if (err) return next(err);
        res.json(event);
      });
  })

  // Update event
  .put(function(req, res, next) {
    Users.findByIdAndUpdate(req.params.id, {
      $set: req.body
    }, {
      new: true
    }, function(err, event) {
      if (err) return next(err);
      res.json(event);
    });
  })

  // Delete event
  .delete(function(req, res, next) {
    Users.findByIdAndRemove(req.params.id, function(err, resp) {
      if (err) return next(err);
      res.json(resp);
    });
  });

// Setup REST API call handlers for '/profiles/:id' - Single object by id
router.route('/:id/videos/choreographer')

  // Get event
  .get(function(req, res, next) {
    Users.findById(req.params.id)
      .populate({
        path: 'videos.choreographer',
        select: 'id music.artist music.title image',
        match: { createdAt: { $lte: req.query.lastItemDate } },
        options: {
          limit: 20
        }
      })
      // TODO: Number of videos
      .exec(function(err, user) {
        if (err) return next(err);
        res.json(user.videos.choreographer);
      });
  });

// Setup REST API call handlers for '/profiles/:id' - Single object by id
router.route('/:id/videos/assistant')

  // Get event
  .get(function(req, res, next) {
    Users.findById(req.params.id)
      .populate({
        path: 'videos.assistant',
        select: 'id music.artist music.title image',
        match: { createdAt: { $lte: req.query.lastItemDate } },
        options: {
          limit: 20
        }
      })
      // TODO: Number of videos
      .exec(function(err, user) {
        if (err) return next(err);
        res.json(user.videos.assistant);
      });
  });

  // Setup REST API call handlers for '/profiles/:id' - Single object by id
  router.route('/:id/videos/dancer')

    // Get event
    .get(function(req, res, next) {
      Users.findById(req.params.id)
        .populate({
          path: 'videos.dancer',
          select: 'id music.artist music.title image',
          match: { createdAt: { $lte: req.query.lastItemDate } },
          options: {
            limit: 20
          }
        })
        // TODO: Number of videos
        .exec(function(err, user) {
          if (err) return next(err);
          res.json(user.videos.dancer);
        });
    });

    // Setup REST API call handlers for '/profiles/:id' - Single object by id
    router.route('/:id/videos/saved')

      // Get event
      .get(function(req, res, next) {
        Users.findById(req.params.id)
          .populate({
            path: 'videos.saved',
            select: 'id music.artist music.title image',
            match: { createdAt: { $lte: req.query.lastItemDate } },
            options: {
              limit: 20
            }
          })
          // TODO: Number of videos
          .exec(function(err, user) {
            if (err) return next(err);
            res.json(user.videos.dancer);
          });
      });

// TODO: Code dupliciation......

module.exports = router;
