var express     = require('express');
var bodyParser  = require('body-parser');
var mongoose    = require('mongoose');

var config  = require('../config/dev');
var Users   = require('../models/user');
// var Verify = require('./verify');

// Init Choreographers Router
var router = express.Router();
router.use(bodyParser.json());

// Setup REST API call handlers for '/choreographers'
router.route('/')

  // List
  .get(function(req, res, next) {
    Choreographers.find({
        createdAt: { $lte: req.query.lastItemDate }
      })
      .populate('choreographer')
      .sort({ createdAt: 'descending' })
      .limit(config.pageCount)
      .exec(function(err, choreographers) {
        if (err) return next(err);
        // http://stackoverflow.com/questions/35114413/how-do-i-obtain-the-count-of-populated-children-in-a-mongoose-model
        // choreographers.dancers.length
        res.json(choreographers);
      });
  })

  // Create
  .post(function(req, res, next) {
    Choreographers.create(req.body, function(err, video) {
      if (err) return next(err);
      console.log('Video created');
      res.json(video)
    });
  })

  // Delete all
  .delete(function(req, res, next) {
    Dishes.remove({}, function(err, resp) {
      if (err) return next(err);
      res.json(resp);
    });
  });

router.route('/search')
  // Search for choreographers by query
  .get(function(req, res, next) {
    Choreographers.find({
        createdAt: { $lte: req.query.lastItemDate }
        // TODO: Find by music, choreographer, description
      })
      .sort({ createdAt: 'descending' })
      .limit(config.pageCount)
      .exec(function(err, choreographers) {
        if (err) return next(err);
        res.json(choreographers);
      });
  })


// Setup REST API call handlers for '/choreographers/:id' - Single object by id
router.route('/:id')

  // Get video
  .get(function(req, res, next) {
    Choreographers.findById(req.params.id)
    .populate('choreographer')
    .populate({
      path: 'assistants',
      select: 'id name username image',
      options: { limit: 5 }
    })
    .populate({
      path: 'dancers',
      select: 'id name username image',
      options: { limit: 5 }
    })
    .exec(function(err, video) {
      if (err) return next(err);
      res.json(video);
    });
  })

  // Update video
  .put(function(req, res, next) {
    Choreographers.findByIdAndUpdate(req.params.id, {
      $set: req.body
    }, {
      new: true
    }, function(err, video) {
      if (err) return next(err);
      res.json(video);
    });
  })

  // Delete video
  .delete(function(req, res, next) {
    Choreographers.findByIdAndRemove(req.params.id, function(err, resp) {
      if (err) return next(err);
      res.json(resp);
    });
  });

router.route('/:id/action')
  .post(function(req, res, next) {
    Choreographers.findById(req.params.id, function(err, video) {
      if (err) return next(err);
      // res.json(dish.comments);
    });
  });

router.route('/:id/likes')
  .get(function(req, res, next) {
    Choreographers.findById(req.params.id, function(err, video) {
      if (err) return next(err);
      // res.json(dish.comments.id(req.params.commentId));
    });
  });

module.exports = router;
