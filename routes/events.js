const express     = require('express');
const bodyParser  = require('body-parser');
const mongoose    = require('mongoose');
const Promise     = require('bluebird');

const config      = require('../config/dev');
const Events      = require('../models/events');
const Locations   = require('../models/locations');
const Videos      = require('../models/videos');
const Verify      = require('./verify');
const Users       = require('../models/user');
const Actions     = require('../models/actions');
const EventAction = Actions.EventAction;
const APIHelper   = require('../config/APIHelper');

// Init Events Router
const router = express.Router();
router.use(bodyParser.json());

// Setup REST API call handlers for '/events'
router.route('/')

  // List
  .get(function(req, res, next) {

    let query = APIHelper.page(req.query.lastItemDate)
    if (req.params.filters && req.params.filters.types) {
      query.type = { $in: req.params.filters.types }
    }
    if (req.params.filters
      && req.params.filters.showPast
      && false == req.body.filters.showPast) {
      query.startDate = { $gt: Date.now }
    }

    Events.find(query)
      .populate({
        path: 'choreographers',
        select: APIHelper.populate.user
      })
      .populate({
        path: 'location',
        select: APIHelper.populate.location
      })
      .sort({ startDate: 'descending' })
      .limit(config.pageCount)
      .execAsync()
      .then(events => {
        // http://stackoverflow.com/questions/35114413/how-do-i-obtain-the-count-of-populated-children-in-a-mongoose-model
        res.json(events);
      })
      .catch(error => { return next(error) });
  })

  // Create
  .post(function(req, res, next) {
    Events.createAsync(req.body)
    .then(event => { res.json(event) })
    .catch(error => { return next(error) });
  })

  // Delete all
  .delete(function(req, res, next) {
    Events.removeAsync({})
    .then(resp => { res.json(resp) })
    .catch(error => { return next(error) });
  });

router.route('/locations')
  .get(function(req, res, next) {
    Locations.find()
    .execAsync()
    .then(locations => { res.json(locations) })
    .catch(error => { return next(error) })
  })
  .post(function(req, res, next) {
    Locations.createAsync(req.body)
    .then(location => { res.json(location) })
    .catch(error => { return next(error) });
  });

router.route('/search')
  // Search for events by query
  .get(function(req, res, next) {

    let query = APIHelper.page(req.query.lastItemDate)
    if (req.query.q) {
      query.$text = { $search : req.query.q }
      query.score = { $meta: 'textScore' }
    }

    Events.find(query)
      .sort({ startDate: 'descending' })
      .limit(config.pageCount)
      .execAsync()
      .then(events => { res.json(events) })
      .catch(error => { return next(error) });
  });

  // TODO: Remove - It's just for testing !!!! REMOVE
  router.route('/actions')

      .get(function(req, res, next) {
        EventAction.find()
        .then(actions => { res.json(actions) })
        .catch(error => { return next(error) });
      });

  router.route('/actions/remove')

        .delete(function(req, res, next) {
          EventAction.remove()
          .then(resp => { res.json(resp) })
          .catch(error => { return next(error) });
    });

// Setup REST API call handlers for '/events/:id' - Single object by id
router.route('/:id')

  // Get event
  .get(function(req, res, next) {
    Events.findById(req.params.id)
    .populate('choreographer location')
    .populate({
      path: 'videos',
      select: APIHelper.populate.video /*,
      options: { limit: 5 }*/
    })
    .execAsync()
    .then(event => {
      // TODO: Number of interests
      res.json(event);
    })
    .catch(error => { return next(error) });
  })

  // Update event
  .put(function(req, res, next) {
    Events.findByIdAndUpdate(
      req.params.id,
      { $set: req.body },
      { new: true }
    )
    .then(event => { res.json(event) })
    .catch(error => { return next(error) });
  })

  // Delete event
  .delete(function(req, res, next) {
    Events.findByIdAndRemove(req.params.id)
    .then(resp => { res.json(resp) })
    .catch(error => { return next(error) });
  });

router.route('/:id/action')
.post(
  Verify.verifyOrdinaryUser,
  function(req, res, next) {

    switch (req.body.action) {
      case 'interests':
        eventsHelper.insertAction(
          req.body.action, // Action
          req.params.id, // Event id
          req.decoded._doc._id // User id
        )
        .then(object => { res.json(APIHelper.success); })
        .catch(error => { return next(error); });
        break;
      case 'uninterest':
        eventsHelper.removeAction(
          req.body.action, // Action
          req.params.id, // Event id
          req.decoded._doc._id // User id
        )
        .then(object => { res.json(APIHelper.success); })
        // If Reference Error: There was no action to undo -> So item is unliked ;)
        .catch(ReferenceError, function(e) { res.json(APIHelper.success); }) // TODO: Megfelelő státuszú response
        .catch(error => { return next(error); });
        break;
      default:
        var err = new Error('Unsupported action');
        err.status = 500;
        return next(err);
    }
    return;
});

router.route('/:id/interests')

  .get(
    Verify.verifyOrdinaryUser,
    function(req, res, next) {
      let interestsCount = 0
      Events.findByIdAsync(req.params.id)
      .then(event => {

        interestsCount = event.interests.length;
        // TODO: [ALL].spread {}
        return event
        .populate({
          path: 'interests',
          select: 'user',
          match: { action: 'interest' },
          options: { limit: config.pageCount }
        })
        .populate({
          path: 'interests.user',
          select: APIHelper.populate.user
        });
      })
      .then(event => { res.json({'count': interestsCount, 'list': event.interests}); })
      .catch(error => { return next(error) });
  });

module.exports = router;


// Private Helper Methods
const eventsHelper = {

  // DB Methods
  insertAction: function(userAction, eventId, userId) {

    return EventAction.findOneAsync({
      user: userId,
      event: eventId,
      action: userAction
    })
    .then(action => {
      // If action has been inserted already -> Return success
      if (action != null) { return Promise.resolve(); }
      // Else insert a new action
      return Promise.all([
        Events.findByIdAsync(eventId),
        Users.findByIdAsync(userId),
        EventAction.createAsync({
          event: eventId,
          user: userId,
          action: userAction
        })
      ])
      .spread(function(event, user, action) {
        if (event == null || user == null || action == null) {
          // TODO: Rollback
          throw new Error("Couldn't perform action");
        }
        switch (userAction) {
          case 'interest':
            event.interests.push(action);
            return event.saveAsync();
          default: return new Promise.resolve();
        }
      });
    });
  },

  removeAction: function(userAction, eventId, userId) {

    return Promise.all([
      Events.findByIdAsync(eventId),
      Users.findByIdAsync(userId),
      EventAction.findOneAndRemoveAsync({
        user: userId,
        event: eventId,
        action: APIHelper.inverseAction(userAction)
      })
    ])
    .spread(function(event, user, action) {
      if (action == null) { throw new ReferenceError('No action found'); }
      if (event == null || user == null) { throw new Error("Couldn't perform action"); }

      switch (userAction) {
        case 'uninterest':
          event.interests.pull(action._id);
          return event.saveAsync();
        default: return new Promise.resolve();
      }
    });
  }
};
