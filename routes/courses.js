var express     = require('express');
var bodyParser  = require('body-parser');
var mongoose    = require('mongoose');

var config  = require('../config/dev');
var Courses = require('../models/courses');
// var Verify = require('./verify');

// Init Courses Router
var router = express.Router();
router.use(bodyParser.json());

// Setup REST API call handlers for '/courses'
router.route('/')

  // List
  .get(function(req, res, next) {
    Courses.find({
        order: { $lte: req.query.lastItemOrder }
      })
      .populate('choreographer location')
      // TODO: Get number of videos and members
      .sort({ order: 'ascending' })
      .limit(config.pageCount)
      .exec(function(err, courses) {
        if (err) return next(err);
        // http://stackoverflow.com/questions/35114413/how-do-i-obtain-the-count-of-populated-children-in-a-mongoose-model
        // courses.dancers.length
        res.json(courses);
      });
  })

  // Create
  .post(function(req, res, next) {
    Courses.create(req.body, function(err, course) {
      if (err) return next(err);
      console.log('Course created');
      res.json(course)
    });
  })

  // Delete all
  .delete(function(req, res, next) {
    Courses.remove({}, function(err, resp) {
      if (err) return next(err);
      res.json(resp);
    });
  });


// Setup REST API call handlers for '/courses/:id' - Single object by id
router.route('/:id')

  // Get course
  .get(function(req, res, next) {
    Courses.findById(req.params.id)
    .populate('choreographer location')
    .populate({
      path: 'videos',
      select: 'id music',
      options: { limit: 5 }
    })
    // TODO: Get number of videos and members
    .exec(function(err, course) {
      if (err) return next(err);
      res.json(course);
    });
  })

  // Update course
  .put(function(req, res, next) {
    Courses.findByIdAndUpdate(req.params.id, {
      $set: req.body
    }, {
      new: true
    }, function(err, course) {
      if (err) return next(err);
      res.json(course);
    });
  })

  // Delete course
  .delete(function(req, res, next) {
    Courses.findByIdAndRemove(req.params.id, function(err, resp) {
      if (err) return next(err);
      res.json(resp);
    });
  });

router.route('/:id/action')
  .post(function(req, res, next) {
    Courses.findByIdAndUpdate(req.params.id, function(err, course) {
      if (err) return next(err);
      // res.json(dish.comments);
    });
  });

router.route('/:id/members')
  .get(function(req, res, next) {
    Courses.findById(req.params.id)
      .populate({
        path: 'members',
        populate: {
          path: 'user',
          select: 'id name image'
        },
        match: { action: 'join' },
        options: { limit: 20 }
      })
      .exec(function(err, course) {
        if (err) return next(err);
        // res.json(dish.comments.id(req.params.commentId));
      });
  });

module.exports = router;
